import modelos.Cliente;
import modelos.Dron;
import modelos.Pedido;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

class AmaDron {
    private static void mostrarInfo() {
        System.out.println("Bienvenido al asistente de AmaDron.");
        System.out.println("¿Que deseas hacer? \n1. Busqueda centralita\n2. Alta de cliente\n3. Alta de pedido\n4.Buscar un cliente\n5. Recorrido del Dron\n6. Si deseas salir");
    }

    void menu() {
        int dni;
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Scanner sc = new Scanner(System.in);
        Cliente cliente = new Cliente();
        Pedido pedido = new Pedido();


        mostrarInfo();

        loop:
        while (true) {
            int option = sc.nextInt();
            switch (option) {
                case 1:

                    break;
                case 2:
                    System.out.println("Introduce el DNI del cliente que quieres dar de alta. (sin letra)");
                    dni = sc.nextInt();

                    int res = cliente.checkUser(dni);
                    if(res == 1) break;

                    System.out.println("Introduce la poblacion del cliente que quieres dar de alta.");
                    String poblacion = sc.next();

                    cliente.altaCliente(dni, poblacion);
                    break;
                case 3:
                    System.out.println("Introduce el nombre del producto");
                    String producto = sc.next();
                    System.out.println("Introduce una descripcion para el producto");
                    String description = sc.next();
                    int tipo = 1;
                    double precio = 2.95;
                    dni = 47577096;
                    pedido.altaPedido(producto, description, tipo, precio, dni, df.format(date));
                    break;
                case 4:
                    System.out.println("Introduce el DNI del usuario que deseas buscar. (sin letra)");
                    dni = sc.nextInt();
                    cliente.getCliente(dni);
                    break;
                case 5:
                    Dron dron = new Dron();
                    dron.recorrido();
                    break loop;
                default:
                    System.out.println("No has introducido una opcion correcta.\n");
                    break;
            }
        }


    }

}
