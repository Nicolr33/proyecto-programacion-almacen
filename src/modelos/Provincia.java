package modelos;

import java.text.Collator;

public class Provincia {
    private String[] provincias = {"Alava","Albacete","Alicante","Almeria","Asturias","Avila","Badajoz","Barcelona","Burgos","Caceres",
            "Cadiz","Cantabria","Castellon","Ciudad Real","Cordoba","La Coruña","Cuenca","Gerona","Granada","Guadalajara",
            "Guipuzcoa","Huelva","Huesca","Islas Baleares","Jaen","Leon","Lerida","Lugo","Madrid","Malaga","Murcia","Navarra",
            "Orense","Palencia","Las Palmas","Pontevedra","La Rioja","Salamanca","Segovia","Sevilla","Soria","Tarragona",
            "Santa Cruz de Tenerife","Teruel","Toledo","Valencia","Valladolid","Vizcaya","Zamora","Zaragoza"};





    /**
     * Verifica si la provincia que se ha introducido para ver si existe en el Array Provincias.
     * @param provincia La provincia que se tiene que verificar
     * @return Si devuelve 1 significa que ha encontrado la provincia en el Array.
     */

    int checkProvincia(String provincia) {
        for (String provincia1 : provincias) {
            if (compareProvincias(provincia, provincia1) == 0) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Compara las provincias sin contar los acentos o caracteres especiales.
     * @return Devuelve 0 si las provincias son iguales.
     */

    private int compareProvincias(String provincia1, String provincia2) {
        final Collator instance = Collator.getInstance();
        instance.setStrength(Collator.NO_DECOMPOSITION);
        return instance.compare(provincia1, provincia2);
    }

    public String[] getAllProvincias() {
        return provincias;
    }

}
