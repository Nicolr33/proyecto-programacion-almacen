package modelos;
import java.util.ArrayList;
import java.util.HashMap;

public class Pedido {
    private HashMap<Integer, ArrayList<String>> pedidos = new HashMap<>();
    private int index = 1;

    public void altaPedido(String producto, String description, int tipo, double precio, int dni, String envio) {
        ArrayList<String> pedido = new ArrayList<>();
        pedido.add(producto);
        pedido.add(description);
        pedido.add(Integer.toString(tipo));
        pedido.add(Double.toString(precio));
        pedido.add(Integer.toString(dni));
        pedido.add(envio);
        pedidos.put(index, pedido);
        index++;
    }

    public void getPedido(int id) {
        ArrayList<String> e = pedidos.get(id);
        for (int i = 0; i < 6; i++) {
            System.out.println(e.get(i));
        }
    }
}
