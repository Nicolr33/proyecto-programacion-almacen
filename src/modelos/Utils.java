package modelos;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.*;

class Utils {

    private static String getJSon(String urlToRead, String city1, String city2) {
        urlToRead = urlToRead + city1 + "|" + city2;
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(urlToRead);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();
        }
        catch (Exception e) {
            return "";
        }
    }

    int getDistance(String city1, String city2)  {
        if(city1.contains(" ")) city1 = city1.replaceAll(" ", "%");
        if(city2.contains(" ")) city2 = city2.replaceAll(" ", "%");

        String ey = getJSon("https://www.distancia.co/route.json?stops=", city1, city2);

        JSONObject obj = new JSONObject(ey);
        JSONArray arr = obj.getJSONArray("distances");

        return extractArr(arr);
    }

    private int extractArr(JSONArray arr) {
        int n = 0;
        for(int i = 0; i < arr.length(); i++) {
            n = arr.getInt(i);
        }
        return n;
    }

}
