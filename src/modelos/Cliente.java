package modelos;
import java.util.HashMap;

public class Cliente {
    private HashMap<Integer, String> clientes = new HashMap<>();



    /**
     * Esta es la funcion para dar de alta a un cliente despues de hacer una serie de comprobaciones
     * @param dni DNI del cliente
     * @param poblacion Población del cliente
     */
    public void altaCliente(Integer dni, String poblacion) {
        Provincia pr = new Provincia();
        int res = pr.checkProvincia(poblacion);

        if(clientes.containsKey(dni)) System.out.println("Este usuario ya esta registrado");

        if(res == 1) {
            poblacion = poblacion.substring(0,1).toUpperCase() + poblacion.substring(1);
            clientes.put(dni, poblacion);
            System.out.println("Se ha guardado el usuario correctamente.");
        } else {
            System.out.println("No se ha encontrado la poblacion");
        }

    }

    /**
     * Se saca informacion del cliente que se ingrese.
     * @param dni DNI del usuario.
     */

    public void getCliente(Integer dni) {
        String poblacion = clientes.get(dni);
        System.out.println("La poblacion del usuario con DNI " + dni + " es " + poblacion);
    }



    /**
     * Verifica si el dni existe en el HashMap
     * @param dni El DNI del usuario
     * @return Si devuelve 1 significa que el usuario existe en el HashMap.
     */

    public int checkUser(int dni) {
        if(clientes.containsKey(dni)) {
            System.out.println("Este usuario ya esta registrado.");
            return 1;
        }
        return 0;
    }
}
