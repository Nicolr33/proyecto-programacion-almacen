package modelos;

import java.util.ArrayList;


public class Dron {
    ArrayList<String> recorrido = new ArrayList<>();
    private Provincia provincia = new Provincia();
    private String[] provincias = provincia.getAllProvincias();
    private Utils utils = new Utils();


    public void recorrido() {
        int i = 0;
        int maxKM = 600;
        String provincia1 = generarProvincia();
        String provincia2;
        int distance;
        recorrido.add(provincia1);

        while (maxKM >= 0) {
            provincia2 = generarProvincia();

            distance = utils.getDistance(provincia1, provincia2);
            if(distance < maxKM) {
                maxKM = maxKM - distance;
                provincia1 = provincia2;
                recorrido.add(provincia2);
            } else {
                i++;
            }

            if(i == 10) break;
        }
        System.out.println(recorrido);
        System.out.println(generateUrl(recorrido));


    }

    private int randomProvincia() {
        return (int) Math.floor(Math.random() * provincias.length);
    }

    private String generarProvincia() {
        return provincias[randomProvincia()];
    }

    private String generateUrl(ArrayList<String> prov) {
        String url = "https://www.google.com/maps/dir/";
        for (int i = 0; i < prov.size(); i++) {
            String prov1 = prov.get(i).replaceAll(" ", "%20");
            url = url.concat(prov1 + "/");
        }
        return url;
    }
}
