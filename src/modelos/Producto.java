package modelos;
import java.util.HashMap;

public class Producto {
    private int id;
    private String nombre;
    private String description;
    private int tipo;
    private double precio;
    private static HashMap<Integer, Producto> productos;

    public Producto(int id, String nombre, String description, int tipo, double precio) {
        this.id = id;
        this.nombre = nombre;
        this.description = description;
        this.tipo = tipo;
        this.precio = precio;
    }

    public static void generarProductos() {
        Producto pr = new Producto(1, "Prueba", "Prueba asd", 1, 2.14);
        productos.put(1, pr);
    }








}
